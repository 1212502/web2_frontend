﻿using Frontend_Ecommic.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace Frontend_Ecommic.Controllers
{
    public class AccountManagerController : Controller
    {
        //
        // GET: /AccountManager/
        public ActionResult Index(ViewModel model)
        {
            if (Request.Cookies.Get("UserName") != null && Request.Cookies.Get("Password") != null)
            {
                if (model.LoginModel == null)
                    model.LoginModel = new LoginViewModel();
                model.LoginModel.UserName = Request.Cookies["UserName"].Value;
                model.LoginModel.Password = Request.Cookies["Password"].Value;
                model.LoginModel.RememberMe = true;
            }
            return View(model);
        }

        public ActionResult DangNhap(ViewModel model, string returnUrl)
        {
            using (HttpClient Client = new HttpClient())
            {
                string url = "api/account/login";
                Client.BaseAddress = new Uri(GLOBAL.urlapi);
                Client.DefaultRequestHeaders.Accept.Clear();
                Client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );
                // get slides
                var value = new { Username = model.LoginModel.UserName, Password = model.LoginModel.Password };
                HttpResponseMessage response = Client.PostAsJsonAsync(url, value).Result;
                if (response.IsSuccessStatusCode)
                {
                    if (model.LoginModel.RememberMe == true)
                    {
                        Response.Cookies["UserName"].Value = model.LoginModel.UserName;
                        Response.Cookies["Password"].Value = model.LoginModel.Password;
                    }
                    var token = response.Content.ReadAsAsync<AccessToken>().Result;
                    Session["Access_Token"] = token;
                    Session.Remove("Google");
                    Session["notice"] = "Đăng nhập thành công!";
                    Session.Remove("LoiDangNhap");
                    Session.Remove("LoiDangKi");
                    Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.access_token);
                    HttpResponseMessage response1 = Client.GetAsync("api/account/IsAdmin").Result;
                    var infor = response1.Content.ReadAsAsync<bool>().Result;
                    if (infor == true)
                    {
                        Session["Admin"] = true;
                    }
                }
                else
                {
                    Session["LoiDangNhap"] = "Thông tin đăng nhập sai!";
                    Session.Remove("LoiDangKi");
                    return RedirectToAction("Index", model);
                }
            }
            return RedirectToAction("Index", "Home");
        }

        public ActionResult DangKi(ViewModel model, string returnUrl)
        {
            if (model.RegisterModel.Password == model.RegisterModel.ConfirmPassword)
            {
                using (HttpClient Client = new HttpClient())
                {
                    string url = "api/account/register";
                    Client.BaseAddress = new Uri(GLOBAL.urlapi);
                    Client.DefaultRequestHeaders.Accept.Clear();
                    Client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json")
                    );
                    // get slides
                    var value = new { Username = model.RegisterModel.UserName, Password = model.RegisterModel.Password, ConfirmPassword = model.RegisterModel.ConfirmPassword, Email = model.RegisterModel.Email, SDT = model.RegisterModel.SDT };
                    HttpResponseMessage response = Client.PostAsJsonAsync(url, value).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var token = response.Content.ReadAsAsync<AccessToken>().Result;
                        Session["Access_Token"] = token;
                        Session.Remove("LoiDangNhap");
                        Session.Remove("LoiDangKi");
                        Session.Remove("Google");
                    }
                    else
                    {
                        Session["LoiDangKi"] = "Thông tin đăng kí sai!";
                        Session.Remove("LoiDangNhap");
                        return RedirectToAction("Index", model);
                    }
                }
            }
            else
            {
                Session["LoiDangKi"] = "Thông tin đăng kí sai!";
                Session.Remove("LoiDangNhap");
                return RedirectToAction("Index", model);
            }
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Dangxuat()
        {
            Session.Remove("Access_Token");
            Session.Remove("Admin");
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Thongtincanhan()
        {
            InforViewModel model = new InforViewModel();
            model.information = new InformationModel();
            model.editPassModel = new EditPassModel();
            AccessToken token = (AccessToken)Session["Access_Token"];
            string a = (string)Session["Doimatkhau"];
            using (HttpClient Client = new HttpClient())
            {
                string url = "api/account/Information";
                Client.BaseAddress = new Uri(GLOBAL.urlapi);
                Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.access_token);
                Client.DefaultRequestHeaders.Accept.Clear();
                Client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );
                // get slides
                HttpResponseMessage response = Client.GetAsync(url).Result;
                if (response.IsSuccessStatusCode)
                {
                    var infor = response.Content.ReadAsAsync<Information>().Result;

                    model.information.UserName = infor.UserName;
                    model.information.Diachi = infor.Diachi;
                    if(infor.Gioitinh != "Nam")
                        model.information.Gioitinh = 1;
                    else
                        model.information.Gioitinh = 0;
                    model.information.HoTen = infor.HoTen;
                    if (infor.Namsinh != null)
                    {
                        string formatted = infor.Namsinh.ToString();
                        model.information.Namsinh = DateTime.Parse(formatted);
                    }
                    if (infor.Ngaydangki != null)
                    {
                        string formatted1 = infor.Ngaydangki.ToString();
                        model.information.Ngaydangki = DateTime.Parse(formatted1);

                    }
                    model.information.SDT = infor.SDT;
                    model.information.Email = infor.Email;
                    model.information.Anhdaidien = infor.Anhdaidien;
                    return View(model);
                }
                else
                {

                }
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Thongtincanhan(InforViewModel model,string UserName, string SDT, string Email, string Anhdaidien)
        {
            model.information.UserName = UserName;
            model.information.SDT = SDT;
            model.information.Email = Email;
            model.information.Anhdaidien = Anhdaidien;
            AccessToken token = (AccessToken)Session["Access_Token"];
            string a = (string)Session["Doimatkhau"];
            using (HttpClient Client = new HttpClient())
            {
                string url = "api/account/EditInformation";
                Client.BaseAddress = new Uri(GLOBAL.urlapi);
                Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.access_token);
                Client.DefaultRequestHeaders.Accept.Clear();
                Client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );
                var value = new { UserName = model.information.UserName, HoTen = model.information.HoTen, Gioitinh = model.information.Gioitinh, Namsinh = model.information.Namsinh, Diachi = model.information.Diachi, SDT = model.information.SDT, Anhdaidien = model.information.Anhdaidien, Ngaydangki = model.information.Ngaydangki, Email = model.information.Email };
                // get slides
                HttpResponseMessage response = Client.PostAsJsonAsync(url, value).Result;
                if (response.IsSuccessStatusCode)
                {

                }
                else
                {

                }
            }
            return RedirectToAction("Thongtincanhan");
        }

        public ActionResult Taikhoangoogle()
        {
            ExternalLoginInfoModels model = new ExternalLoginInfoModels();
            if (Session["Google"] != null)
            {
                var infor = (ExternalLoginInfo)Session["Google"];
                model.DefaultUserName = infor.DefaultUserName;
                model.Email = infor.Email;
                model.ExternalIdentity = infor.ExternalIdentity;
                model.Login = infor.Login;
            }

            return View(model);
        }

        public ActionResult Thaydoimatkhau(InforViewModel model)
        {
            if (model.editPassModel.Password != model.editPassModel.ConfirmPassword)
            {
                Session["Doimatkhau"] = "Mật khẩu không khớp!";
            }
            else
            {
                AccessToken token = (AccessToken)Session["Access_Token"];
                using (HttpClient Client = new HttpClient())
                {
                    string url = "api/account/ChangeCurrentPassword";
                    Client.BaseAddress = new Uri(GLOBAL.urlapi);
                    Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.access_token);
                    Client.DefaultRequestHeaders.Accept.Clear();
                    Client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json")
                    );
                    // get slides
                    //var value = new { NewPassword = model.editPassModel.Password, CurrentPassword = model.editPassModel.OldPass };
                    //HttpResponseMessage response = Client.PostAsJsonAsync(url, value).Result;
                    var value = new { NewPassword = model.editPassModel.Password, CurrentPassword = model.editPassModel.OldPass };
                    HttpResponseMessage response = Client.PostAsJsonAsync(url, value).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var newtoken = response.Content.ReadAsAsync<AccessToken>().Result;
                        Session["Access_Token"] = newtoken;
                        Session.Remove("Doimatkhau");
                        Session.Remove("Doithongtin");
                        Session["Doimatkhau"] = "Thay đổi mật khẩu thành công!";
                    }
                    else
                    {
                        Session["Doimatkhau"] = "Mật khẩu sai!";
                    }
                }
            }
            return RedirectToAction("Thongtincanhan", "AccountManager");
        }
	}
}