﻿using Frontend_Ecommic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Frontend_Ecommic.Controllers
{
    public class SanphamController : Controller
    {
        // GET: Sanpham
        public ActionResult DanhSachSanPham(string name)
        {
            var temp = GLOBAL.getLoai(name);
            return View(temp);
        }
        public ActionResult PartialSanpham()
        {
            return View();
        }
        public ActionResult Chitietsanpham(string ID)
        {
            try
            {
                SanphamModel model = new SanphamModel();
                var temp = model.GetChiTietSanPham(Int32.Parse(ID));
                return View(temp);
            }
            catch (Exception e)
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        public ActionResult Sanphamlienquan(string ID)
        {
            SanphamModel model = new SanphamModel();
            var temp = model.GetSanPhamLQ(ID);
            return View(temp);
        }
    }
}