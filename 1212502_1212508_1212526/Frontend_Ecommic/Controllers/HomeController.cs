﻿using Frontend_Ecommic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Frontend_Ecommic.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            IndexModel model = new IndexModel();
            var temp = model.GetThongTinIndex();
            return View(temp);
        }
        public ActionResult Menu()
        {
            MenuModel model = new MenuModel();
            var temp = model.getDsMenu();
            return PartialView("Menuweb",temp);
        }
        public ActionResult Thongtinchitietsanpham ()
        {
            return View();
        }
        public ActionResult Sanpham()
        {
            return View();
        }
        public ActionResult Quanligiohang()
        {
            return View();
        }
        public ActionResult ThongBao()
        {
            return View();
        }
    }
}