﻿using Frontend_Ecommic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Frontend_Ecommic.Controllers
{
    public class GiohangController : Controller
    {
        // GET: Giohang
        public JsonResult Themgiohang(string id_SP,string soLuong, string Color)
        {
            GiohangModel model = new GiohangModel();
            model.Themgiohang(id_SP, soLuong, Color);
            float soluong = model.soLuongGioHang();
            return Json(soluong, JsonRequestBehavior.AllowGet);
        }
        public ActionResult xemGioHang()
        {
            return View();
        }
        public JsonResult xemGioHangangular()
        {
            return Json(GLOBAL.Giohang, JsonRequestBehavior.AllowGet);
        }
        public JsonResult changeColor(string color,string ID_SP)
        {
            GiohangModel model = new GiohangModel();
            model.ChangeColor(color, ID_SP);
            return Json("Oke", JsonRequestBehavior.AllowGet);
        }
        public ActionResult changequanlity(string soLuong, string ID_SP)
        {
            GiohangModel model = new GiohangModel();
            model.ChangeQuanlity(soLuong, ID_SP);
            return PartialView("_thongtingiohang");
        }
        public ActionResult XoaCart(string ID_SP)
        {
            GiohangModel model = new GiohangModel();
            model.xoaCart(ID_SP);
            return PartialView("_thongtingiohang");
        }
        
    }
}