﻿using Frontend_Ecommic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Frontend_Ecommic.Controllers
{
    public class SendMailController : Controller
    {
        //
        // GET: /SendMail/
        public ActionResult Index()
        {
            var model = new ContactModels();
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(ContactModels model)
        {
            if (ModelState.IsValid)
            {
                string smtpUserName = "eoooihic@gmail.com";
                string smtpPassword = "eimqcrgljzhzdwwn";
                string smtpHost = "smtp.gmail.com";
                int smtpPort = 25;

                string emailTo = "eooihic@gmail.com"; // Khi có liên hệ sẽ gửi về thư của mình
                string subject = model.Subject;
                string body = string.Format("Bạn vừa nhận được liên hệ từ: <b>{0}</b><br/>Email: {1}<br/>Nội dung: </br>{2}",
                    model.UserName, model.Email, model.Message);

                EmailService service = new EmailService();

                bool kq = service.Send(smtpUserName, smtpPassword, smtpHost, smtpPort,
                    emailTo, subject, body);

                if (kq) ModelState.AddModelError("", "Cảm ơn bạn đã liên hệ với chúng tôi.");
                else ModelState.AddModelError("", "Gửi tin nhắn thất bại, vui lòng thử lại.");
            }
            return View(model);
        }
	}
}