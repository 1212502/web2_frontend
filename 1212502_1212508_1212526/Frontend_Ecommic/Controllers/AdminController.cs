﻿using Frontend_Ecommic.Attributes;
using Frontend_Ecommic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using PagedList;
namespace Frontend_Ecommic.Controllers
{
    [myAuthorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Sanpham (int page = 1, int pagesize = 10)
        {
            DSSanPham model = new DSSanPham();
            string url = "api/sanpham/layALLSanPham";
            HttpResponseMessage response = CallAPi(url, null);
            if (response.IsSuccessStatusCode)
            {
                model.sanPham = response.Content.ReadAsAsync<List<SanphamModel>>().Result;
            }
            else
            {

            }
            var myVal = new PagedList<SanphamModel>(model.sanPham, page, pagesize); 
            //foreach (MyModel m in data) 
            //{ 
            //    myVal.Add(new MyViewModel(m)); 
            //} 
            //return myVal; 
            //model.sanPham.ToPageList()
            return View(myVal);
        }

        public ActionResult Loaisanpham()
        {
            LoaiSanPhamModel loaisp = new LoaiSanPhamModel();
            DSLoaisanPham model = new DSLoaisanPham();
            model.Loaisanpham = loaisp.getAllTypeProduct();
            return View(model);
        }

        public ActionResult Chinhsualoaisanpham(LoaisanphamModels model)
        {
            string url = "api/admin/LoaiSanPham/Edit";
            if (model.ID == null || model.Loai == null)
            {
                return RedirectToAction("loaisanpham", "admin");
            }
            var token = (AccessToken)Session["Access_Token"];
            CallAPi(url, token, model);
            return View(model);
        }

        public ActionResult Themmoiloaisanpham()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Themmoiloaisanpham(ThemloaisanphamModels model)
        {
            string url = "api/admin/LoaiSanPham/Add";
            if (!String.IsNullOrEmpty(model.Loai))
            {
                var token = (AccessToken)Session["Access_Token"];
                CallAPi(url, token, model);
                return RedirectToAction("loaisanpham", "admin");
            }
            return View(model);
        }

        public ActionResult Xoaloaisanpham(int id)
        {
            string url = "api/admin/LoaiSanPham/Delete";
            var token = (AccessToken)Session["Access_Token"];
            CallAPi(url, token, new { ID = id });
            return RedirectToAction("Loaisanpham");
        }

        public ActionResult EditSanpham()
        {
            return View();
        }

        public ActionResult NSX()
        {
            LoaiSanPhamModel loaisp = new LoaiSanPhamModel();
            DSNSX model = new DSNSX();
            string url = "api/sanpham/AllNSX";
            HttpResponseMessage response = CallAPi(url, null);
            if (response.IsSuccessStatusCode)
            {
                model.nsx = response.Content.ReadAsAsync<List<NSXModel>>().Result;
            }
            else
            {

            }
            return View(model);
        }

        public ActionResult Chinhsuansx(nsxModels model)
        {
            string url = "api/admin/Nhasanxuat/Edit";
            if (model.ID == null || model.Ten == null)
            {
                return RedirectToAction("nsx", "admin");
            }
            var token = (AccessToken)Session["Access_Token"];
            CallAPi(url, token, model);
            return View(model);
        }

        public ActionResult Xoansx(int id)
        {
            string url = "api/admin/Nhasanxuat/Delete";
            var token = (AccessToken)Session["Access_Token"];
            CallAPi(url, token, new { ID = id });
            return RedirectToAction("nsx");
        }

        public ActionResult Themmoinsx()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Themmoinsx(themnsxModels model)
        {
            string url = "api/admin/Nhasanxuat/Add";
            if (!String.IsNullOrEmpty(model.Ten))
            {
                var token = (AccessToken)Session["Access_Token"];
                CallAPi(url, token, model);
                return RedirectToAction("nsx", "admin");
            }
            return View(model);
        }

        public ActionResult Table()
        {
            return View();
        }

        public HttpResponseMessage CallAPi(string url, AccessToken token)
        {
            using (HttpClient Client = new HttpClient())
            {
                Client.BaseAddress = new Uri(GLOBAL.urlapi);
                if(token != null)
                {
                    Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.access_token);
                }
                Client.DefaultRequestHeaders.Accept.Clear();
                Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = Client.GetAsync(url).Result;
                return response;
            }
        }

        public void CallAPi(string url, AccessToken token, Object obj)
        {
            using (HttpClient Client = new HttpClient())
            {
                Client.BaseAddress = new Uri(GLOBAL.urlapi);
                if (token != null)
                {
                    Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.access_token);
                }
                Client.DefaultRequestHeaders.Accept.Clear();
                Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = Client.PostAsJsonAsync(url, obj).Result;
            }
        }

        public ActionResult Thongbaothongtin()
        {
            return View();
        }

    }
}