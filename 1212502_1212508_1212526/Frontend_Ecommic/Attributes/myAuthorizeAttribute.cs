﻿using Frontend_Ecommic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Frontend_Ecommic.Attributes
{
    [AttributeUsageAttribute(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class myAuthorizeAttribute : AuthorizeAttribute
    {

        //Custom named parameters for annotation
        public string ResourceKey { get; set; }
        public string OperationKey { get; set; }

        //Called when access is denied
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            //User isn't logged in
            AccessToken token;

            if (filterContext.HttpContext.Session["Access_Token"] != null)
            {
                token = (AccessToken)filterContext.HttpContext.Session["Access_Token"];
                using (HttpClient Client = new HttpClient())
                {
                    string url = "api/account/IsAdmin";
                    Client.BaseAddress = new Uri(GLOBAL.urlapi);
                    Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.access_token);
                    Client.DefaultRequestHeaders.Accept.Clear();
                    Client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json")
                    );
                    // get slides
                    
                    HttpResponseMessage response = Client.GetAsync(url).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var infor = response.Content.ReadAsAsync<bool>().Result;
                        if(infor != true)
                        {
                            filterContext.Result = new RedirectToRouteResult(
                                    new RouteValueDictionary(new { controller = "AccountManager", action = "Index" })
                            );
                        }
                    }
                    else
                    {
                        filterContext.Result = new RedirectToRouteResult(
                                new RouteValueDictionary(new { controller = "AccountManager", action = "Index" })
                        );
                    }
                }
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary(new { controller = "AccountManager", action = "Index" })
                );
            }
        }

        //Core authentication, called before each action
        //protected override bool AuthorizeCore(HttpContextBase httpContext)
        //{
        //    var b = myMembership.Instance.Member().IsLoggedIn;
        //    //Is user logged in?
        //    if (b)
        //        //If user is logged in and we need a custom check:
        //        if (ResourceKey != null && OperationKey != null)
        //            return ecMembership.Instance.Member().ActivePermissions.Where(x => x.operation == OperationKey && x.resource == ResourceKey).Count() > 0;
        //    //Returns true or false, meaning allow or deny. False will call HandleUnauthorizedRequest above
        //    return b;
        //}
    }
}