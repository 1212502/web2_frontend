﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Frontend_Ecommic.Models
{
    public class MenuModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public List<SubMenuModel> subMenu { get; set; }

        public List<MenuModel> getDsMenu()
        {
            using (HttpClient Client = new HttpClient())
            {
                List<MenuModel> model = new List<MenuModel>();
                string url = "api/getMenu";
                Client.BaseAddress = new Uri(GLOBAL.urlapi);
                Client.DefaultRequestHeaders.Accept.Clear();
                Client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );
                // get slides
                HttpResponseMessage response = Client.GetAsync(url).Result;
                if (response.IsSuccessStatusCode)
                {
                    model = response.Content.ReadAsAsync<List<MenuModel>>().Result;
                    return model;
                }
                return model;
            }
        }
    }
    public class SubMenuModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public Nullable<int> Id_menu { get; set; }
        public string Action { get; set; }
    }
}