﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Frontend_Ecommic.Models
{
    public class GLOBAL
    {
        public static string urlapi = "http://localhost:21366/";
        public static List<LoaiSanPhamModel> DsLoai = null;
        public static List<GiohangModel> Giohang = new List<GiohangModel>();
        public static GiohangModel gioHangCurrent = new GiohangModel();
        public static LoaiSanPhamModel getLoai(string type)
        {
            if(DsLoai == null)
            {
                LoaiSanPhamModel model = new LoaiSanPhamModel();
                DsLoai = model.getAllTypeProduct();
            }
            return DsLoai.Where(m => m.TenLoai == type).FirstOrDefault();
        }
    }
}