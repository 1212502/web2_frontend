﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Frontend_Ecommic.Models
{
    public class ViewModel
    {
        public LoginViewModel LoginModel { get; set; }
        public RegisterViewModel RegisterModel { get; set; }
    }

    public class InforViewModel
    {
        public InformationModel information { get; set; }
        public EditPassModel editPassModel { get; set; }
    }
}