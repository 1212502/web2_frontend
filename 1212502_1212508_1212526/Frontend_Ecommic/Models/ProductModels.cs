﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Frontend_Ecommic.Models
{
    public class LoaisanphamModels
    {
        [Required]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "Loại sản phẩm")]
        public string Loai { get; set; }
    }

    public class ThemloaisanphamModels
    {
        [Required]
        [Display(Name = "Loại sản phẩm")]
        public string Loai { get; set; }
    }

    public class nsxModels
    {
        [Required]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "Nhà sản xuất")]
        public string Ten { get; set; }
    }

    public class themnsxModels
    {
        [Required]
        [Display(Name = "Nhà sản xuất")]
        public string Ten { get; set; }
    }
}