﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Frontend_Ecommic.Models
{
    public class IndexModel
    {
        public List<SliderModel> Sliders;
        public List<SanphamModel> sanPhamMoi;
        public List<SanphamModel> sanPham;
        public List<LoaiSanPhamModel> loaiSanPham;

        public IndexModel GetThongTinIndex()
        {
            using(HttpClient Client = new HttpClient())
            {
                IndexModel model = new IndexModel();
                string url = "api/getIndex";
                Client.BaseAddress = new Uri(GLOBAL.urlapi);
                Client.DefaultRequestHeaders.Accept.Clear();
                Client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );
                // get slides
                HttpResponseMessage response = Client.GetAsync(url).Result;
                if(response.IsSuccessStatusCode)
                {
                    model = response.Content.ReadAsAsync<IndexModel>().Result;
                    return model;
                }
                return null;
            }
        }
    }
    public class SliderModel
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public string Linktoaction { get; set; }
    }
}