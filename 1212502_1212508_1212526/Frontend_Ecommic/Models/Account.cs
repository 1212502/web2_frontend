﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace Frontend_Ecommic.Models
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "UserName")]
        [EmailAddress]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "UserName")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long {1}.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [RegularExpression(@"(09)\d{8}|(01)\d{9}", ErrorMessage = "Entered phone format is not valid.")]
        public string SDT { get; set; }
    }

    public class InformationModel
    {
        public string UserName { get; set; }

        public string Anhdaidien { get; set; }
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Display(Name = "Diachi")]
        public string Diachi { get; set; }

        [Display(Name = "Gioitinh")]
        public int Gioitinh { get; set; }

        [Display(Name = "Hoten")]
        public string HoTen { get; set; }

        [Display(Name = "Namsinh")]
        [DataType(DataType.DateTime)]
        public DateTime Namsinh { get; set; }

        [Display(Name = "Ngaydangki")]
        public DateTime Ngaydangki { get; set; }

        [Required]
        [RegularExpression(@"(09)\d{8}|(01)\d{9}", ErrorMessage = "Entered phone format is not valid.")]
        public string SDT { get; set; }
    }

    public class Information
    {
        public string UserName { get; set; }

        public string HoTen { get; set; }

        public string Gioitinh { get; set; }

        public DateTime? Namsinh { get; set; }

        public string Diachi { get; set; }

        public string SDT { get; set; }

        public string Anhdaidien { get; set; }

        public DateTime? Ngaydangki { get; set; }

        public string Email { get; set; }
    }

    public class EditPassModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu cũ")]
        public string OldPass { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu mới")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Nhập lại mật khẩu")]
        public string ConfirmPassword { get; set; }
    }

    public class ExternalLoginInfoModels
    {
        // Summary:
        //     Suggested user name for a user
        public string DefaultUserName { get; set; }
        //
        // Summary:
        //     Email claim from the external identity
        public string Email { get; set; }
        //
        // Summary:
        //     The external identity
        public ClaimsIdentity ExternalIdentity { get; set; }
        //
        // Summary:
        //     Associated login data
        public UserLoginInfo Login { get; set; }
    }
}