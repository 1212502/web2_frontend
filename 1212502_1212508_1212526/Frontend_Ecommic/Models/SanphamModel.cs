﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Frontend_Ecommic.Models
{
    public class SanphamModel
    {
        public int ID { get; set; }
        public string TenSanPham { get; set; }
        public string Duongdanhinh { get; set; }
        public Nullable<double> Giatien { get; set; }
        public string Motasanpham { get; set; }
        public Nullable<int> ID_Loai { get; set; }
        public Nullable<int> Soluong { get; set; }
        public string NSX { get; set; }
        public string TenNSX { get; set; }
        public Nullable<System.DateTime> Ngaydang { get; set; }
        public List<ColorModel> Color { get; set; }

        public SanphamModel GetChiTietSanPham(int id)
        {
            using (HttpClient Client = new HttpClient())
            {
                SanphamModel model = new SanphamModel();
                string url = "api/sanpham/laymotsanpham?IDSP=" + id;
                Client.BaseAddress = new Uri(GLOBAL.urlapi);
                Client.DefaultRequestHeaders.Accept.Clear();
                Client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );
                // get slides
                HttpResponseMessage response = Client.GetAsync(url).Result;
                if (response.IsSuccessStatusCode)
                {
                    model = response.Content.ReadAsAsync<SanphamModel>().Result;
                    return model;
                }
                return model;
            }
        }
        public List<SanphamModel> GetAllSanPham()
        {
            using (HttpClient Client = new HttpClient())
            {
                List<SanphamModel> model = new List<SanphamModel>();
                string url = "api/sanpham/layALLSanPham";
                Client.BaseAddress = new Uri(GLOBAL.urlapi);
                Client.DefaultRequestHeaders.Accept.Clear();
                Client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );
                // get slides
                HttpResponseMessage response = Client.GetAsync(url).Result;
                if (response.IsSuccessStatusCode)
                {
                    model = response.Content.ReadAsAsync<List<SanphamModel>>().Result;
                    return model;
                }
                return model;
            }
        }
        public List<SanphamModel> GetSanPhamLQ(string ID)
        {
            using (HttpClient Client = new HttpClient())
            {
                List<SanphamModel> model = new List<SanphamModel>();
                string url = "api/sanpham/Sanphamlienquan?ID=" + ID;
                Client.BaseAddress = new Uri(GLOBAL.urlapi);
                Client.DefaultRequestHeaders.Accept.Clear();
                Client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );
                // get slides
                HttpResponseMessage response = Client.GetAsync(url).Result;
                if (response.IsSuccessStatusCode)
                {
                    model = response.Content.ReadAsAsync<List<SanphamModel>>().Result;
                    return model;
                }
                return model;
            }
        }
        

    }
    /// <summary>
    /// Class color
    /// </summary>
    public class ColorModel
    {
        public int ID { get; set; }
        public string Ten { get; set; }
        public Nullable<int> ID_sanpham { get; set; }
    }
}