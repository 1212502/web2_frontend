﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Frontend_Ecommic.Models
{
    public class DonhangModel
    {
        public string MaDH { get; set; }
        public Nullable<int> SoLuong { get; set; }
        public Nullable<decimal> ThanhTien { get; set; }
        public SanphamModel sanPham { get; set; }

       
    }
}