﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Frontend_Ecommic.Models
{
    public class GiohangModel
    {
        public string Id_SP { get; set; }
        public int soLuong { get; set; }
        public string Color { get; set; }
        public SanphamModel sanPham { get; set; }
        public void Themgiohang(string ID_SP,string soLuong,string Color)
        {
            try
            {
                GiohangModel temp;
                if(GLOBAL.Giohang.Count(m=>m.Id_SP == ID_SP) == 0)
                {
                    SanphamModel model = new SanphamModel();
                    model = model.GetChiTietSanPham(Int32.Parse(ID_SP));
                    temp = new GiohangModel()
                    {
                        Id_SP = ID_SP,
                        soLuong = Int32.Parse(soLuong),
                        Color = Color,
                        sanPham = model
                    };
                    GLOBAL.Giohang.Add(temp);
                }
                else
                {
                    temp = GLOBAL.Giohang.Where(m => m.Id_SP == ID_SP).FirstOrDefault();
                    temp.soLuong += Int32.Parse(soLuong);
                }
                
                
            }
            catch (Exception e) { }
        }
        public float soLuongGioHang()
        {
            float count = 0;
            foreach(var item in GLOBAL.Giohang)
            {
                count += item.soLuong;
            }
            return count;
        }
        public double tongTienCart()
        {
            double count = 0;
            foreach (var item in GLOBAL.Giohang)
            {
                count += (double)item.sanPham.Giatien * item.soLuong;
            }
            return count;
        }
        public void ChangeColor(string Color,string ID_SP)
        {
            try
            {
                GiohangModel temp = GLOBAL.Giohang.Where(m => m.Id_SP == ID_SP).FirstOrDefault();
                temp.Color = Color;
            }
            catch (Exception e) { }
        }

        public void ChangeQuanlity(string soLuong, string ID_SP)
        {
            try
            {
                GiohangModel temp = GLOBAL.Giohang.Where(m => m.Id_SP == ID_SP).FirstOrDefault();
                temp.soLuong = Int32.Parse(soLuong);
            }
            catch (Exception e) { }
        }

        internal void xoaCart(string ID_SP)
        {
            try
            {
                GiohangModel temp = GLOBAL.Giohang.Where(m => m.Id_SP == ID_SP).FirstOrDefault();
                GLOBAL.Giohang.Remove(temp);
            }
            catch (Exception e) { }
        }
    }
    public class Cartinfo
    {
        public double soLuong { get; set; }
        public double tongTien { get; set; }
        public double tienShip { get; set; }
        public double Total { get; set; }
    }
}