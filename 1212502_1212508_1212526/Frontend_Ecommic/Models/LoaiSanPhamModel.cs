﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Frontend_Ecommic.Models
{
    public class LoaiSanPhamModel
    {
        public int ID { get; set; }
        public string TenLoai { get; set; }
        public List<LoaiSanPhamModel>getAllTypeProduct()
        {
            using (HttpClient Client = new HttpClient())
            {
                List<LoaiSanPhamModel> model = new List<LoaiSanPhamModel>();
                string url = "api/sanpham/loaiSanPham";
                Client.BaseAddress = new Uri(GLOBAL.urlapi);
                Client.DefaultRequestHeaders.Accept.Clear();
                Client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );
                // get slides
                HttpResponseMessage response = Client.GetAsync(url).Result;
                if (response.IsSuccessStatusCode)
                {
                    model = response.Content.ReadAsAsync<List<LoaiSanPhamModel>>().Result;
                    return model;
                }
                return model;
            }
        }
    }
}