﻿var url = "http://localhost:21366/";
//var myapp = angular.moduler("myapp", []);
//myapp.factory("getSanPham", function ($http) {
//    var sanphamService = {};
//    sanphamService.getSanPham = function(skip,take)
//    {
//        return $http.get(url + "api/sanpham/laySanPhamAt?skip=" + skip + "&take=" + take);
//    }
//    return sanphamService;
//});
//angular.module('myapp').controller('indexcontrollerangular', function ($scope, getSanPham) {
//    alert("hi");
//    $scope.title = "hello";
//    getSanPham.getSanPham(0, 1).success(function (result) {
//        $scope.sanPham = result;
//        alert(result);
//    });
//});

angular.module('myapp', ['ui.bootstrap']);
angular.module('myapp').factory("getSanPham", function ($http) {
    var baseurl = "http://localhost:21366/";
    var sanphamService = {};
    sanphamService.getSanPham = function (skip, take,type) {
        return $http.get(baseurl + "api/sanpham/sanphamtheotype?type=" + type + "&skip=" + skip + "&take=" + take);
    }
    sanphamService.getAllSanPham = function(type)
    {
        return $http.get(baseurl + "api/sanpham/layALLSanPhamTheoLoai?type=" + type);
    }
    sanphamService.getLoaiSanPham = function () {
        return $http.get(baseurl + "api/sanpham/loaiSanPham");
    }
    sanphamService.getNSX = function () {
        return $http.get(baseurl + "api/sanpham/AllNSX");
    }
    sanphamService.Timkiem = function (param1,param2,param3) {
        return $http.get(baseurl + "api/sanpham/TimkiemFor?ID_Loai=" + param1 + "&nameNSX=" + param2 + "&nameProduct=" + param3);
    }
    sanphamService.gettintuc = function () {
        return $http.get(baseurl + "api/RSS");
    }
    return sanphamService;
});


// gio hang

angular.module('myapp').factory("Giohang", function ($http) {
    var baseurl = "http://localhost:21366/";
    var giohangService = {};
    giohangService.themGioHang = function (ID, soLuong, Color) {
        return $http.get("/Giohang/Themgiohang?id_SP=" + ID + "&soLuong=" + soLuong + "&Color=" + Color);
    }
    giohangService.ChangeColor = function (Color,ID) {
        return $http.get("/Giohang/changeColor?color=" + Color + "&ID_SP=" + ID);
    }
    giohangService.changequantity = function (soLuong,ID) {
        return $http.get("/Giohang/changequanlity?soLuong=" + soLuong + "&ID_SP=" + ID);
    }
    giohangService.xoaCart = function (ID) {
        return $http.get("/Giohang/XoaCart?ID_SP=" + ID);
    }
    giohangService.getCart = function () {
        return $http.get("/Giohang/xemGioHangangular");
    }
    return giohangService;
});

angular.module('myapp').directive('repeatDone', function () {
    return function (scope, element, attrs) {
        if (scope.$last) { // all are rendered
            scope.$eval(attrs.repeatDone);
        }
    }
});
angular.module('myapp').filter('range', function () {
    return function (input, total) {
        total = parseInt(total);

        for (var i = 1; i <= total; i++) {
            input.push(i);
        }

        return input;
    };
});

