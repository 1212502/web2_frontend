﻿angular.module('myapp').controller('xuligiohang', function ($scope, $http, Giohang) {

    Giohang.getCart().success(function (result) {
        $scope.giohangcurrent = result;
    })
    $scope.submitCart = function (param)
    {
        Giohang.themGioHang(param,$scope.cart.quality, $scope.cart.color).success(function (response)
        {
            alert("Them thanh cong");
            $(".last").html(response);
        })
    }
    $scope.changeColor = function(item)
    {
        Giohang.ChangeColor(item.Color, item.Id_SP).success(function (response) {
            
        })
    }
    $scope.changequantity = function(item)
    {
        Giohang.changequantity(item.sl, item.Id_SP).success(function (response) {
            $(".mycartinfo").html(response);
        })
    }
    $scope.addtoCart = function (param) {
        Giohang.themGioHang(param,'1','Trắng').success(function (response) {
            alert("Them thanh cong");
            $(".last").html(response);
        })
    }
    $scope.xoaCart = function(item,index)
    {
        Giohang.xoaCart(item.Id_SP).success(function (response) {
            $scope.giohangcurrent.splice(index, 1);
            $(".mycartinfo").html(response);
        })
    }
    
});