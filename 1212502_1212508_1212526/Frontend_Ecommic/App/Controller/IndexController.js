﻿angular.module('myapp').controller('mycontroll', function ($scope, $http, getSanPham) {
    $scope.isend = false;
    getSanPham.getSanPham(0, 4, 1).success(function (result) {
        $scope.sanPham = result;
    });

    $scope.doSomething = function () {
        $scope.isend = "true";
    };
    $scope.changetype = function (param, param1) {
        $scope.select = param1;
        getSanPham.getSanPham(0, 4, param).success(function (result) {
            $scope.sanPham = result;
        });
    }
    $scope.isSelected = function (param) {
        return $scope.select == param;
    }
});
