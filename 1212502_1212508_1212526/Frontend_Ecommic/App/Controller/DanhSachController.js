﻿angular.module('myapp').controller('MyControllersp', function ($scope, $http, getSanPham) {
    $scope.todos = [];
    $scope.current = [];
    $scope.LoaiSP = "";
    $scope.hangSX = "";
    $scope.khoitao = function (param) {
        getSanPham.getAllSanPham(param).success(function (response) {
            $scope.todos = response;
            $scope.totalItems = Math.ceil($scope.todos.length / $scope.itemsPerPage) * 10;
        });
    }
    getSanPham.getLoaiSanPham().success(function (response) {
        $scope.typeProduct = response;
    });
    getSanPham.getNSX().success(function (response) {
        $scope.typeNSX = response;
    });

    
    $scope.totalItems = 10;
    $scope.currentPage = 1;
    $scope.maxSize = 5;
    $scope.itemsPerPage = 4;
    
    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };


    $scope.pageChanged = function () {
        console.log('Page changed to: ' + $scope.currentPage);
    };


    $scope.setItemsPerPage = function (num) {

        $scope.itemsPerPage = num;

        $scope.currentPage = 1; //reset to first paghe

    }
    $scope.tang = false;
    $scope.value = "Giá tăng";
    //sort
    $scope.sort = function()
    {
        if($scope.value == "Giá tăng")
        {
            $scope.tang = false;
        }
        else
        {
            $scope.tang = true;
        }
    }
    $scope.Search = function()
    {
        if ($scope.LoaiSP == "" && $scope.hangSX == "")
        {
            
            return false;
        }
        getSanPham.Timkiem($scope.LoaiSP, $scope.hangSX,"").success(function (response) {
            $scope.todos = response;
            $scope.totalItems = Math.ceil($scope.todos.length / $scope.itemsPerPage) * 10;
        });
    }
    
});