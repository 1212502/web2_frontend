﻿/// <reference path="Search.js" />
angular.module('myapp').controller('Search', function ($scope, $http, getSanPham) {
    // gives another movie array on change
    $scope.demo = ["Lord of the Rings",
                        "Drive",
                        "Science of Sleep",
                        "Back to the Future",
                        "Oldboy"];
    // gives another movie array on change
    $scope.updateMovies = function (typed) {
        if (typed == "") {
            $scope.demo = [];
        }
        else {
            getSanPham.Timkiem("", "", typed).success(function (response) {
                $scope.demo = response;
            });
        }
    }
});
